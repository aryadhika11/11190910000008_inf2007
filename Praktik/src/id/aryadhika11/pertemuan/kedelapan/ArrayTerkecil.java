/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class ArrayTerkecil {
    int i, min;

    public int getMin(int A[], int n) {
        min = 9999;
        for (i = 0; i < n; i++) {
            if (A[i] < min) {
                min = A[i];
            }
        }
        return min;
    }

    public static void main(String[] args) {

        int i, n;

        ArrayTerkecil arrayTerkecil = new ArrayTerkecil();
        Scanner in = new Scanner(System.in);

        System.out.print("N = ");
        n = in.nextInt();

        int A[] = new int[n];

        for (i = 0; i < A.length; i++) {
            System.out.print("masukkan array [" + i + "] = ");
            A[i] = in.nextInt();
        }
        System.out.println("elemen terkecil = " + arrayTerkecil.getMin(A, n));
    }

}

