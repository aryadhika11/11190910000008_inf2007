/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class PenambahanMatriks {

    int i, j;

    public int[][] getPenambahanMatriks(int A[][], int B[][], int nBar, int nKol) {
        int[][] C = new int[nBar][nKol];
        Scanner in = new Scanner(System.in);
        for (i = 0; i < nBar; i++) {
            for (j = 0; j < nKol; j++) {
                C[i][j] = A[i][j] + B[i][j];
            }
        }
        System.out.println("Hasil penjumlahan Array");
        for (i = 0; i < C.length; i++) {
            for (j = 0; j < C.length; j++) {
                System.out.println(C[i][j] + "  ");
            }
            System.out.println("  ");
        }
        return C;
    }

    public static void main(String[] args) {
        PenambahanMatriks penambahanMatriks = new PenambahanMatriks();
        int i, j, nBar, nKol;
        Scanner in = new Scanner(System.in);
        System.out.println("Masukkan Baris = ");
        nBar = in.nextInt();
        System.out.println("Masukkan Kolom = ");
        nKol = in.nextInt();

        int[][] A = new int[nBar][nKol];
        int[][] B = new int[nBar][nKol];
        int[][] C = new int[nBar][nKol];

        System.out.println("Masukkan Nilai A = ");
        for (i = 0; i < A.length; i++) {
            for (j = 0; j < A.length; j++) {
                System.out.println("Masukkan Array A [" + i + "," + j + "] = ");
                A[i][j] = in.nextInt();

                System.out.println("Masukkan Nilai B");
                for (i = 0; i < B.length; i++) {
                    for (j = 0; j < B.length; j++) {
                        System.out.println("Masukkan Array B [" + i + "," + j + "] = ");
                        B[i][j] = in.nextInt();
                        
                        
                    }
                }
                penambahanMatriks.getPenambahanMatriks(A, B, nBar, nKol);
            }

        }

    }
}
