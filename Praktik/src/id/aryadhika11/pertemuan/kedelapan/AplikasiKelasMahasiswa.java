/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class AplikasiKelasMahasiswa {

    public static void main(String[] args) {
        int jumlahdata;
        int nim;
        String nama;
        double nilai;

        Scanner in = new Scanner(System.in);
        System.out.println("Masukkan Jumlah Array = ");
        jumlahdata = in.nextInt();

        Mahasiswa[] mahasiswas = new Mahasiswa[jumlahdata];
        for (int i = 0; i <= jumlahdata - 1; i++) {
            System.out.println("Mahasiwa ke = " + (i + 1));
            System.out.println("Masukkan NIM = ");
            nim = in.nextInt();
            System.out.println("Masukkan Nama = ");
            nama = in.next();
            System.out.println("Masukkan Nilai");
            nilai = in.nextDouble();

            mahasiswas[i] = new Mahasiswa(nim, nama, nilai);
        }
        System.out.println("Data Mahasiswa Pada Array");
        for (Mahasiswa mahasiswa : mahasiswas){
            System.out.println("NIM = " + mahasiswa.getNim() + "\tNama : " + mahasiswa.getNama() + "\tNilai : " +mahasiswa.getNilai());                
        }
    }
}
