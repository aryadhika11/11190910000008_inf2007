/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.kedelapan;

import java.util.Scanner;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class CariArray {
   int i, j = -1;

    public int getCari(int nilai[], int angka) {
        System.out.println("nilai pada Array:");
        for (i = 0; i < nilai.length; i++) {
            System.out.print(nilai[i] + "  ");
        }

        System.out.println("");
        for (i = 0; i < nilai.length; i++) {
            if (angka == nilai[i]) {
                j = i; 
                System.out.print("angka yang dicari (" + angka + ")");
                System.out.println(" berada pada indeks ke: " + j);
             
            }
        }
        if (j == -1) {
                System.out.println("0");
        }
        return j;
    }

    public static void main(String[] args) {
        int x, n, i, j;
        Scanner in = new Scanner(System.in);
        CariArray cariArray = new CariArray();

        System.out.print("N = ");
        n = in.nextInt();
       int[] nilai = new int[n];

        for (i = 0; i < nilai.length; i++) {
            System.out.print("masukkan array [" + i + "] : ");
            nilai[i] = in.nextInt();
        }

        System.out.print("masukkan angka yang dicari: ");
        x = in.nextInt();

        cariArray.getCari(nilai, n);
        
    }
}


