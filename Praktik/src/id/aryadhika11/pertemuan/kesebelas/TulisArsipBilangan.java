/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.kesebelas;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;
import java.io.IOException;


/**
 *
 * @author Muhammad Arya Dhika
 */
public class TulisArsipBilangan {
    public static void main(String[] args) {
//        String Bil = "C:\\TI 19\\temp\\Tulis.txt";
        int i, n;
        Scanner in = new Scanner(System.in);
        
        try {
            PrintWriter outFile = new PrintWriter(new FileOutputStream("D:\\Documents\\DDP\\TulisArsipBilangan.txt"));
            System.out.print("masukkan n: ");
            n = in.nextInt();

            for (i = 1; i <= n; i++) {
                outFile.println(i);
            }
            outFile.close();

        } catch (IOException e) {
            System.err.println("Error : " + e.getMessage());
        }
    }
}
