/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.kesebelas;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class ArsipTeksAn {

    boolean getCariKarakter(FileReader B) {
        boolean ketemu = false;
        char[] L;
        Scanner line = new Scanner(new BufferedReader(B));
        int n = 0;
        while (line.hasNext()) {
            L = line.next().toCharArray();
            for (int i = 0; i < L.length; i++) {
                if (L[i] == 'a') {
                    if (L[i + 1] == 'n') {
                        ketemu = true;
                    }
                }
            }

        }
        return ketemu;
    }

    public static void main(String[] args) {
        ArsipTeksAn baca = new ArsipTeksAn();
        boolean a;
        try {
            a = baca.getCariKarakter(new FileReader("D:\\Documents\\DDP\\ArsipTeksAn.txt"));
            System.out.println("Ada kata an = " + a);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ArsipTeksAn.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

