/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.ketiga;
import java.util.Scanner;
/**
 *
 * @author Muhammad Arya Dhika
 */
public class SelisihDuaTanggal {
    public static void main(String args[]) {
        int dd1, mm1, yy1, dd2, mm2, yy2, dd3, mm3 , yy3;
        int totalhari1, totalhari2, selisihhari, sisa;
        
        Scanner input = new Scanner(System.in);
        
        System.out.println("Masukan hari pertama");
        dd1 = input.nextInt();
        System.out.println("Masukan bulan pertama");
        mm1 = input.nextInt();
        System.out.println("Masukan tahun pertama");
        yy1 = input.nextInt();
        
        System.out.println("Masukan hari kedua");
        dd2 = input.nextInt();
        System.out.println("Masukan bulan kedua");
        mm2 = input.nextInt();
        System.out.println("Masukan tahun kedua");
        yy2 = input.nextInt();
        
        totalhari1 = (yy1 * 365) + (mm1*30) + dd1; // untuk menghitung total hari input pertama
        totalhari2 = (yy2 * 365) + (mm2*30) + dd2; // untuk menghitung total hari input kedua
        
        selisihhari = totalhari2 - totalhari1; // selisih antara inputan pertama dan kedua
        
        yy3 = selisihhari/365; // menentukan tahun hasil selisih
        sisa = selisihhari % 365; 
        mm3 = sisa/30; // menentukan bulan hasil selisih
        dd3 = sisa % 30; // menentukan hari hasil selisih
        
        System.out.println("hasil nya   " + yy3 + ("/") + mm3 + ("/") + dd3);
    }
}
        
        
    

