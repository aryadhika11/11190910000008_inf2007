/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class ElemenTerakhir {

    private int i;
    private int n;

    public int getElemenTerakhir(int L[], int n, int x) {
        i = n-1; //sequential
        boolean ketemu;
        ketemu = false;
        while ((i > -1) && (i <= n-1) && (!ketemu)) {
            if (i <= n-1) {
                System.out.println("posisi ke" + i + "" + "isi" +L[i]);
            }
            if (L[i] == x) {
                ketemu = true ;
            }else{
                i = i - 1;
            }
        }
        if (ketemu) {
            return i;
        }else{
            return -1;
        }
        }
    
    public static void main(String[] args) {
        ElemenTerakhir elemenTerakhir = new ElemenTerakhir();
        int L[] = {13, 16, 14, 21, 76, 15};
        int n = 6;
        Scanner in = new Scanner(System.in);
        System.out.println("input nilai x = ");
        int x = in.nextInt();
        System.out.println("" + elemenTerakhir.getElemenTerakhir(L, n, x));
    }
}
