/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class SequentialSearch {

    private int i;

    public boolean getSeqSearchBoolean(int L[], int n, int x) {
        i = 0;
        while ((i < n ) && (L[i] != x)) {
            if (i == 0) {
                System.out.println("posisi ke = " + i + "isinya" + L[i]);
            }
            i = i + 1;
            System.out.println("posisi ke = " + i + "isinya " + L[i]);
        }
        if (L[i] == x) {
            return true;
        }
        return false;
    }

    public int getSeqSearchIndeks(int L[], int n, int x) {
        i = 0;
        while ((i < n - 1) && (L[i] != x)) {
            if (i == 0) {
                System.out.println("posisi ke = " + i + "isinya" + L[i]);
            }
            i = i + 1;
            System.out.println("posisi ke = " + i + "isinya " + L[i]);
        }
        if (L[i] == x) {
            return i;
        }else{
        return -1;
        }
    }

    public boolean getSeqSearchInBoolean(int L[], int n, int x) {
        i = 0;

        boolean ketemu = false;
        while ((i < n) && (!ketemu)) {
            if (L[i] == x) {
                ketemu = true;
            }
            i = i + 1;
        }
        return ketemu;
    }

    public int getSeqSearchInIndeks(int L[], int n, int x) {
        i = 0;
        boolean ketemu;
        ketemu = false;
        while ((i < n) && (!ketemu)) {
            if (L[i] == x) {
                ketemu = true;
            }
            i = i + 1;
        }
        if (ketemu) {
            return i;
        }
        return -1;
    }

    public int getBinarySearch(int L[], int n, int x) {
        int idx;
        L[n] = x;    //sentinel
        i = 0;
        while (L[i]!= x) {
            i = i + 1;
        }
        if (i == n) {
            return idx = -1;
        }
        return idx = 1;
    }

    
    public static void main(String[] args) {
        SequentialSearch searchBoolean = new SequentialSearch();
        //int L[] = {13, 16, 14, 21, 76, 15};
        int L[] = new int [6];
        L[0] = 13;
        L[1]=16;
        L[2]=14;
        L[3]=21;
        L[4]=76;
        L[5]=15;
        int n = 6;
        Scanner in = new Scanner(System.in);
        System.out.println("input nilai x = ");
        int x = in.nextInt();
        //System.out.println("idx =  " + searchBoolean.getSeqSearchBoolean(L, n, x));
        System.out.println("" + searchBoolean.getSeqSearchIndeks(L, n, x));
        //System.out.println("" + searchBoolean.getSeqSearchInBoolean(L, n, x));
        //System.out.println("" + searchBoolean.getSeqSearchInIndeks(L, n, x));
        //System.out.println("" + searchBoolean.getBinarySearch(L, n, x));
    }
}
