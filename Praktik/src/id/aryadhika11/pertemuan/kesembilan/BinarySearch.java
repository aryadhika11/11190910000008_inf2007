/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class BinarySearch {

    private int i, j, k;
    private boolean ketemu;

    public int getBinarySearch(int L[], int n, int x) {
        i = 0;
        j = n;
        boolean ketemu = false;
        while ((!ketemu) && (i <= j)) {
            k = (i + j) / 2;
            if (L[k] == x) {
                ketemu = true;
            } else {
                if (L[k] < x) {
                    i = k + 1;
                }else{
                j = k - 1;
            }
            }
            }
        i = i + 1;
        if (ketemu) {
            
            return k;
        }
        return -1;
    }
    public static void main(String[] args) {
       Scanner in = new Scanner(System.in);
       int L []= {1, 2, 3, 4, 5, 6, 7};
       int n = 6;
       
       BinarySearch binarySearch = new BinarySearch();
        System.out.println("masukkan nilai x = ");
        int x = in.nextInt();
        System.out.println("" + binarySearch.getBinarySearch(L, n, x));
        System.out.println();
    }
}
