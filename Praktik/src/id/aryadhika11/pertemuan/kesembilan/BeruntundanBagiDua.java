/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.kesembilan;

import java.util.Scanner;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class BeruntundanBagiDua {

    private int i;

    public int getBeruntun(String L[], int n, String x) {
        i = 0;
        while ((i < n - 1) && (!(L[i].equals(x)))) {
            if (i == 0) {
                System.out.println("posisi ke = " + i + "isinya" + L[i]);
            }
            i = i + 1;
            System.out.println("posisi ke = " + i + "isinya " + L[i]);
        }
        if (L[i].equals(x)) {
            return i;
        } else {
            return -1;
        }
    }

    public int getBagiDua(String L[], int n, String x) {
        i = 0;
        int k = 0;
        int j = n-1;
        boolean ketemu;
        ketemu = false;
        while ((!ketemu) && (i <= j)) {
            k = (i + j) / 2;
            if (L[k].equals(x)) {
                ketemu = true;
            } else {
                if (L[k].compareTo(x) < 0) {
                    i = k + 1;
                } else {
                    j = k - 1;
                }
            }
        }
        if (ketemu) {
            return k;
        } else {
            return -1;
        }
    }


    public static void main(String[] args) {
        BeruntundanBagiDua beruntundanBagiDua = new BeruntundanBagiDua();
        Scanner in = new Scanner(System.in);
        int n = 5;
        String L[] = {"Dika", "Arya", "Sasha", "Yupi", "Indi"};
        System.out.println("masukkan karakter");
        String x = in.nextLine();
       // System.out.println("index = " + beruntundanBagiDua.getBeruntun(L, n, x));
        System.out.println("index = " + beruntundanBagiDua.getBagiDua(L, n, x));
    }
}
