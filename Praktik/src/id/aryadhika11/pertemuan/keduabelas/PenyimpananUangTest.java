/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.keduabelas;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class PenyimpananUangTest {
    public static void main(String[] args) {
        PenyimpananUang tabungan = new PenyimpananUang (5000, 8.5 / 100);
        System.out.println("uang yang ditabung = 5000");
        System.out.println("Tingkat bunga sekarang = 8.5%");
        System.out.println("Total uang anda sekarang = " + tabungan.cekUang ());
    }
}
