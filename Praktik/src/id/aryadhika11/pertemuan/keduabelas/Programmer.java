/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.keduabelas;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class Programmer extends Pegawai {

    private int bonus;

    public Programmer(String nama, int gaji, int bonus) {
        this.nama = nama;
        this.gaji = gaji;
        this.bonus = bonus;
    }

    @Override
    public int infogaji() {
        return gaji;
    }

    public int infobonus() {
        return bonus;

    }
}
