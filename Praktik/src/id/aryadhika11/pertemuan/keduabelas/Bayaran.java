/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.keduabelas;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class Bayaran {
    public int hitungbayaran (Pegawai pegawai){
        int uang =  pegawai.infogaji();
        if (pegawai instanceof Manager){
            uang += ((Manager) pegawai).infotunjangan();
        }else if (pegawai instanceof Programmer) {
        uang += ((Programmer )pegawai).infobonus();
        } 
        return uang;            
        }
    public static void main(String[] args) {
        Manager m = new Manager ("Budi", 800, 50);
        Programmer p = new Programmer ("cecep", 600, 30);
        Bayaran upah = new Bayaran ();
        System.out.println("upah manager = " + upah.hitungbayaran(m) );
        System.out.println("upah Programmer = " + upah.hitungbayaran(p));   
        }
    }
    
