/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.keduabelas;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class PenyimpananUang extends Tabungan {

    private double tingkatbunga;

    public PenyimpananUang(int saldo, double tingkatbunga) {
        super(saldo);
        this.tingkatbunga = tingkatbunga;
    }
    public double  cekUang(){
        return saldo + (saldo * tingkatbunga);
        
        
}
}
