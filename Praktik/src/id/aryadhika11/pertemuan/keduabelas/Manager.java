/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.keduabelas;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class Manager extends Pegawai {

    private int tunjangan;

    public Manager(String nama, int gaji, int tunjangan) {
        this.nama = nama;
        this.gaji = gaji;
        this.tunjangan = tunjangan;
    }

    public int infogaji() {
        return gaji;
    }

    public int infotunjangan() {
        return tunjangan;
    }
}
