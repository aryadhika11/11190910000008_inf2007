/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.kesepuluh;

import java.util.Arrays;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class BubbleSortLatihan {

    private static int i, j;
    private int temp;

    public int[] getBubbleSortLatihan(int L[], int n) {
       for (i = 0; i < n - 1; i++) {
            for (j = n - 1; j > i; j--) {
                 System.out.println("");
                System.out.println("i = " + i + ", k = " + (j - 1) + "-->" + L[j - 1]);
                if (L[j] > L[j - 1]) {
                    temp = L[j];
                    L[j] = L[j - 1];
                    L[j - 1] = temp;
                }
            }
        }
        
        return L;
    }
    public static void main(String[] args) {
        int L[] = {41, 3, 2,1,5,6};
        int n = L.length;
        BubbleSortLatihan arya = new BubbleSortLatihan();
        System.out.println("Angka Belum terurut");
        System.out.println(Arrays.toString(L));
        arya.getBubbleSortLatihan(L, n);
        System.out.println("Angka Setelah urut");
        System.out.println(Arrays.toString(L));
        
    }
}
