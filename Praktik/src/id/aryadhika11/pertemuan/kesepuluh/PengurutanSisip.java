/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.kesepuluh;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class PengurutanSisip {

    private int i, j, y;
    private boolean ketemu;

    public int[] getInsertionSort(int L[], int n) {
        for (i = 0; i < n; i++) {
            y = L[i];
            j = i - 1;
            ketemu = false;
            while ((j >= 0) && (!ketemu)) {
                if (y < L[j]) {
                    L[j + 1] = L[j];
                    j = j - 1;
                } else {
                    ketemu = true;
                }
            }
            L[j + 1] = y;
        }
        return L;
    }

    public static void main(String[] args) {
        PengurutanSisip dhika = new PengurutanSisip();
        Scanner in = new Scanner(System.in);
        int L[] = {2, 3, 4, 5, 8, 7};
        int n = L.length;
        System.out.println("Angka sebelum urut");
        System.out.println(Arrays.toString(L));
        dhika.getInsertionSort(L, n);
        System.out.println("Angka setelah urut");
        System.out.println(Arrays.toString(L));

    }
}
