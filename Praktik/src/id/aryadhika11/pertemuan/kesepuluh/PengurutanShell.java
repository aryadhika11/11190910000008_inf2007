/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.kesepuluh;

import java.util.Arrays;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class PengurutanShell {

    private int i, j, y, step, start;
    private boolean ketemu;

    public int[] getShellSort(int L[], int n) {
        step = n-1;
        while (step > 1) {
            step = (step / 3) + 1;
            for (start = 0; start < step; start++) {
                i = start + step;
                while (i <= n-1) {
                    y = L[i];
                    j = i - step;
                    ketemu = false;
                    while ((j >= 0) && (!ketemu)) {
                        if (y < L[j]) {
                            L[j + step] = L[j];
                            j = j - step;
                        } else {
                            ketemu = true;
                        }
                    }

                    L[j + step] = y;
                    i = i + step;
                }
            }
        }
        return L;
    }
    

    public static void main(String[] args) {
        PengurutanShell dhika = new PengurutanShell();
        int L[]= {25, 27, 10, 8, 76, 21};
        int n = L.length;
        System.out.println("angka sebelum urut");
        System.out.println(Arrays.toString(L));
        dhika.getShellSort(L, n);
        System.out.println("angka setelah urut");
        System.out.println(Arrays.toString(L));
    }
    }
