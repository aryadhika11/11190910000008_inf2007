/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.kesepuluh;

import java.util.Arrays;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class SeleksiSort {

    private int i, j, imin, temp;
    private int imaks;

    public int[] getSelesctionSortMin(int L[], int n) {
        for (i = 0; i< n - 1; i++) {
            imin = i;
            for (j = i ; j <= n-1; j++) {
                if (L[j] < L[imin]) {
                    imin = j;
                }
            }
            temp = L[i];
            L[i] = L[imin];
            L[imin] = temp;
        }
        return L;
    }

    public int[] getSelectionSortMax(int L[], int n) {
        for (i = n-1; i > 0; i--) {
            imaks = 1;
            for (j = 0; j < i; j++) {
                if (L[j] > L[imaks]) {
                    imaks = j;
                }
            }
            temp = L[i];
            L[i] = L[imaks];
            L[imaks] = temp;
        }
        return L;
    }

    public static void main(String[] args) {
        SeleksiSort arya = new SeleksiSort();
        int L[] = {25, 27, 10, 8, 76, 21};
        int n = L.length;
        System.out.println("Angka sebelum urut");
        System.out.println(Arrays.toString(L));
        arya.getSelectionSortMax(L, n);
//        arya.getSelesctionSortMin(L, n);
        System.out.println("Angka setelah urut");
        System.out.println(Arrays.toString(L));
        
    }

}
