/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.kesepuluh;

import java.util.Arrays;

/**
 *
 * author Muhammad Arya Dhika
 */
public class BubbleSort {

    private static int n, i, k;

    private int temp;

    public int[] getBubbleSort(int L[], int n) {
        for (i = 0; i < n - 1; i++) {
            for (k = n - 1; k > i; k--) {
                System.out.println("");
                System.out.println("i = " + i + ", k = " + (k - 1) + "-->" + L[k - 1]);
                if (L[k] < L[k - 1]) {
                    temp = L[k];
                    L[k] = L[k - 1];
                    L[k - 1] = temp;
                }
            }
        }

        return L;
    }

    public static void main(String[] args) {
        int L[] = {2, 4, 5, 6, 7, 8, 9, 12, 1};
        int n = L.length;
        BubbleSort arya = new BubbleSort();
        System.out.println("Angka Sebelum urut");
        System.out.println(Arrays.toString(L));
        arya.getBubbleSort(L, n);
        System.out.println("Angka setelah urut");
        System.out.println(Arrays.toString(L));

    }
}
