/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.ketujuh;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class PersegiPanjang {

    double panjang;
    private double lebar;

    public PersegiPanjang() {
        System.out.println("Konstruktor persegi panjang");
    }


    public  void PersegiPanjang(double panjang, double lebar) {
        this.panjang = panjang;
        this.lebar = lebar;
    }

    public double getLuas() {
        return panjang * lebar;
    }

    public double getKeliling() {
        return 2 * (panjang + lebar);
    }

    public void getInfo() {
        System.out.println("Kelas Persegi Panjang");
    }
}
