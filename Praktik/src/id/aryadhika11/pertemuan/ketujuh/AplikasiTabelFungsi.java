/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class AplikasiTabelFungsi {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        float x = in.nextFloat();
        Fungsi fungsi = new Fungsi();
        System.out.println("----------------");
        System.out.println("  x    f(x)     ");
        System.out.println("----------------");
        x = 10;
        while (x <= 15) {
            System.out.println("x,'       ', f(x)");
            x = (float) (x + 0.2);
        }
        System.out.println("--------------");

    }
}
