/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class AplikasiPersegiPanjang {
    public static void main(String[] args) {
        //mecoba konstruktor default
        //get mengambil dri perintah awal
        //this menyimpan variabel
        PersegiPanjang persegiPanjang2 = new PersegiPanjang();
        
        Scanner in = new Scanner(System.in);
        double panjang, lebar;
        
        System.out.println("Masukkan panjang");
        panjang = in.nextDouble();
        
        System.out.println("Masukkan Lebar");
        lebar = in.nextDouble();
        
        PersegiPanjang persegiPanjang = new PersegiPanjang();
        persegiPanjang.getInfo();
        System.out.println("Luas = " + persegiPanjang.getLuas());
        System.out.println("Keliling = " + persegiPanjang.getKeliling());
    }
}
