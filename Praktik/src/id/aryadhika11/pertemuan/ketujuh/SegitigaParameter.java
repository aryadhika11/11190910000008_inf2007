/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.ketujuh;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class SegitigaParameter {
    private final double alas;
    private final double tinggi;
    private double luas;
    
    public SegitigaParameter (double alas, double tinggi) {
        this.alas = alas;
        this.tinggi = tinggi;
    }
    public double Luas () {
        return luas = (alas * tinggi) / 2;
    }
}

