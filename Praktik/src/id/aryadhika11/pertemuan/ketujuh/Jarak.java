/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.ketujuh;

import static java.lang.Math.sqrt;
import java.util.Scanner;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class Jarak {

    public int X1, X2, Y1, Y2;

    public double getJarak(double Jarak) {
        Scanner in = new Scanner(System.in);
        System.out.println("Masukkan nilai X1");
        X1 = in.nextInt();
        System.out.println("Masukkan nilai X2");
        X2 = in.nextInt();
        System.out.println("Masukkan nilai Y1");
        Y1 = in.nextInt();
        System.out.println("Masukkan nilai Y2");
        Y2 = in.nextInt();
        return Jarak = sqrt((X1 - X2) * (X1 - X2) + (Y1 - Y2) * (Y1 - Y2));
    }
}
