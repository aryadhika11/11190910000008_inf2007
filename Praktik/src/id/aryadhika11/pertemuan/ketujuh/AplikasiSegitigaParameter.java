/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.ketujuh;

import java.util.Scanner;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class AplikasiSegitigaParameter {
    public static void main(String[] args) {
        int i;
        Scanner in = new Scanner(System.in);
        System.out.println("Masukkan banyak segitiga");
        int N = in.nextInt();
        System.out.println("Masukkan Alas");
        int alas = in.nextInt();
        System.out.println("Masukkan Tinggi");
        int tinggi = in.nextInt();
        for (i = 0; i < N; i++) {
             SegitigaParameter segitiga = new SegitigaParameter (alas, tinggi);
             System.out.println("Luas Segitiga = " + segitiga.Luas());
        }
    }
}

