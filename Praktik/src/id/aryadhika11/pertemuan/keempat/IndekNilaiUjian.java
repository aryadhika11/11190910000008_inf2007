/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.keempat;

import java.util.Scanner;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class IndekNilaiUjian {

    public static void main(String[] args) {
        char indeks;
        Scanner in = new Scanner(System.in);
        int nilai = in.nextInt();
        if (nilai >= 80) {
            indeks = 'A';
        } else {
            if ((nilai >= 70) && (nilai < 80)) {
                indeks = 'B';
            } else {
                if ((nilai >= 55) && (nilai < 70)) {
                    indeks = 'C';
                } else {
                    if ((nilai >= 40) && (nilai < 50)) {
                        indeks = 'D';
                    } else {
                        indeks = 'E';
                    }
                    System.out.println(indeks);
                }
            }
        }
    }
}
