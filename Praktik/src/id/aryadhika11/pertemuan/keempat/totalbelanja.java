/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.keempat;

import java.util.Scanner;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class totalbelanja {
     public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int totalbelanja = in.nextInt();
        int diskon, nilaibelanja;

        if (totalbelanja > 120000) {
            diskon = totalbelanja * 7 / 100;
            nilaibelanja = totalbelanja - diskon;
            System.out.println("Diskon = " + diskon + "\n" + "nilai belanja = " + nilaibelanja);
        } else {
            nilaibelanja = totalbelanja;
            System.out.println("nilai belanja = " + nilaibelanja);
        }
    }
}
