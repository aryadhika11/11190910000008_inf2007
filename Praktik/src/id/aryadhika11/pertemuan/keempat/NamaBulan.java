/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.keempat;

import java.util.Scanner;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class NamaBulan {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int NoBulan = in.nextInt();
        switch (NoBulan) {
            case 1:
                System.out.println("januari");
                break;
            case 2:
                System.out.println("febuari");
                break;
            case 3:
                System.out.println("maret");
                break;
            case 4:
                System.out.println("april");
                break;
            case 5:
                System.out.println("mei");
                break;
            case 6:
                System.out.println("juni");
                break;
            case 7:
                System.out.println("juli");
                break;
            case 8:
                System.out.println("agustus");
                break;
            case 9:
                System.out.println("september");
                break;
            case 10:
                System.out.println("oktober");
                break;
            case 11:
                System.out.println("november");
                break;
            case 12:
                System.out.println("desember");
                break;
            default:
                System.out.println("bukan bulan yang benar");
        }
    }
}
