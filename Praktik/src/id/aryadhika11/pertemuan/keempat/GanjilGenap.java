/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.keempat;
import java.util.Scanner;
/**
 *
 * @author Muhammad Arya Dhika
 */
public class GanjilGenap {
    public static void main(String[] args) {
         Scanner in = new Scanner(System.in);
         int x = in.nextInt();
         if ( x % 2 == 0) {
             System.out.println("genap");
         } else {
             System.out.println("ganjil");
         }
    }
}
