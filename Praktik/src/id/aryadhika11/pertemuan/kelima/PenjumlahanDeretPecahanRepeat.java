/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhka11.pertemuan.kelima;

import java.util.Scanner;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class PenjumlahanDeretPecahanRepeat {

    public static void main(String[] args) {
        int x;
        float s;
        s = 0;
        Scanner in = new Scanner(System.in);
        x = in.nextInt();
        do {
            s = s +  1 / x;
            x = in.nextInt();
        } while (x != -1);
        System.out.println(s);
    }
}
