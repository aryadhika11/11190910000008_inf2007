/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhka11.pertemuan.kelima;

import java.util.Scanner;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class FaktorialFor {
    public static void main(String[] args) {
        int fak;
        int i;
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        fak = 1;
        for (i =1; i <= n; i++){
            fak = fak * i;
        }
        System.out.println(fak);
        }
    }

