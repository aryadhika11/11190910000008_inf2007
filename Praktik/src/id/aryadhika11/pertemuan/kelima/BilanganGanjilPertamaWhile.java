/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhka11.pertemuan.kelima;

import java.util.Scanner;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class BilanganGanjilPertamaWhile {

    public static void main(String[] args) {
        int i, jumlah;
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        jumlah = 0;
        i = 1;
        while (i <= N) {
            jumlah = jumlah + i;
            i = i + 1;
        }
        System.out.println(jumlah);

    }

}

