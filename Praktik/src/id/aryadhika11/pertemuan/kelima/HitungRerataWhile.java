/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhka11.pertemuan.kelima;

import java.util.Scanner;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class HitungRerataWhile {

    public static void main(String[] args) {
        float rerata;
        int jumlah = 0;
        int i = 0;
        Scanner in = new Scanner(System.in);
        int x = in.nextInt();
        while (x != -1) {
            i = i + 1;
            jumlah = jumlah + x;
             x = in.nextInt();
        }
        if (i != 0) {
            rerata = jumlah / i;
            System.out.println(rerata);
        } else {
            System.out.println("tidak ada nilai ujian yang dimasukkan");
        }

    }

}
