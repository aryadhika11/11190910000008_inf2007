/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.kedua;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class classDivide {

    public static void main(String[] arguments) {
        float number1 = 15;
        float number2 = 6;
        float result = number1 / number2;
        float remainder = number1 % number2;
        System.out.println(number1 + " divided by " + number2);
        System.out.println("\nResult\tReminder");
        System.out.println(result + "\t" + remainder);
    }
}
