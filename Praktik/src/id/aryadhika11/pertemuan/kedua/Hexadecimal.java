/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.kedua;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class Hexadecimal {
    public static void main(String args[]) {
        int x = 0x001;
        int y = 0x7fffffff;
        int z = 0xDeadCafe;
        System.out.println("x = " + x);
        System.out.println("y = " + y);
        System.out.println("z = " + z);
    }
}
