/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.kedua;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class Octal {
    public static void main(String args[]) {
        int six = 06;
        int seven = 07;
        int eight = 010;
        int nine = 011;
        System.out.println("Octal six = " + six);
        System.out.println("Octal seven = " + seven);
        System.out.println("Octal eight = " + eight);
        System.out.println("Octal nine = " + nine);
    }
}            

