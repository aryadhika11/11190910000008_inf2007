/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.keenam;

import javax.swing.JOptionPane;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class InputJOptionPaneEx {

    public static void main(String[] args) {
        int bilangan;
        String box = JOptionPane.showInputDialog("Masukan Bilangan: ");

        bilangan = Integer.parseInt(box);

        System.out.println("Bilangan = " + bilangan);

    }
}
