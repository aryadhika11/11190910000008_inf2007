/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.keenam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


/**
 *
 * @author Muhammad Arya Dhika
 */
public class InputStreamReaderEx {
    public static void main(String[] args) {
        int bilangan;
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        
        System.out.println("Masukkan Bilangan");
        try {
            bilangan = Integer.parseInt(in.readLine());
            
            System.out.println("bilangan = " + bilangan);
        } catch (IOException ex) {
            System.out.println("error = " + ex.toString());
        }
    }
}
