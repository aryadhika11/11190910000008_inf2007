/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.keenam;

import javax.swing.JOptionPane;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class OutputJOptionPaneEx {
    public static void main(String[] args) {
        int bilangan;
        String box = JOptionPane.showInputDialog("Masukkan bilangan: ");
        
        bilangan = Integer.parseInt(box);
        
        JOptionPane.showMessageDialog(null, "Bilangan = " + bilangan, "Hasil Input", JOptionPane.INFORMATION_MESSAGE);
    }
}
