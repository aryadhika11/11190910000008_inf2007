/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.keenam;

import java.util.Scanner;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class HandlingThrow {

    public static void main(String[] args) {
        try {
            System.out.println("Masukkan Angka: ");
            Scanner in = new Scanner(System.in);
            int num = in.nextInt();
            if (num > 10) {
                throw new Exception();
            }
            System.out.println("angka kurang dari atau sama dengan 10");
        } catch (Exception err) {
            System.out.println("angka lebih dari 10");

            System.out.println("selesai");
        }
    }
}
