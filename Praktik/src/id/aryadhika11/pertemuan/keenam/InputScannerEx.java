/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.keenam;
import java.util.Scanner;
/**
 *
 * @author Muhammad Arya Dhika
 */
public class InputScannerEx {
    public static void main(String[] args) {
        int bilangan;
        Scanner in = new Scanner(System.in);
        System.out.println("Masukkan Bilangan :");
        bilangan = in.nextInt();
        
        System.out.println("bilangan = " + bilangan);
    }
}
