/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.pertemuan.pertama;
import java.util.Scanner;
/**
 *
 * @author Muhammad Arya Dhika
 */
public class FahrenheitCelcius {
/* PROGRAM FahrenheitCelcius */
/* Program untuk mencetak table Fahrenheit-Celcius dari x sampai y
dengan kenaikan sebesar step. Masukan program ini adalah suhu awal,
suhu akhir, step, dan keluarannya adalah table konversi suhu dalam C dan F */
	public static void main(String[] args) {
		/* DEKLARASI */
		float F, C;
		int x, y, step;
		
		/* ALGORITMA */
		Scanner in = new Scanner(System.in);
		x = in.nextInt();
		y = in.nextInt();
		step = in.nextInt();
		
		F = x;
		while  (F <= y) {
			System.out.println("5/9 : " + 5/9);
			//C = (5 / 9) * (F - 32);
			//C = (float) (5.0 / 9.0) * (F - 32);
			C = 5 * (F - 32) / 9;
			System.out.printf("%3.0f  %6.1f \n", F, C);
			F = F + step;
		}
	}
}


