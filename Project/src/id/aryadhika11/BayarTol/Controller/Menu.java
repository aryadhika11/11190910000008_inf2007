/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.BayarTol.Controller;

import id.aryadhika11.BayarTol.Model.Info;
import java.util.Scanner;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class Menu {
    private final Scanner in = new Scanner(System.in);
    private int noMenu;

    public void getMenuAwal() {
        Info info = new Info();

        System.out.println("=================================================");
        System.out.println(info.getAplikasi());
        System.out.println(info.getVersion());
        System.out.println("-------------------------------------------------");
        
        System.out.println("||**SELAMAT DATANG DI GERBANG TOL PERTAMA**||");
        System.out.println("DAFTAR MENU MASUK GERBANG TOL");
        System.out.println("1. Masuk Tol");
        System.out.println("2. Laporan Harian");
        System.out.println("3. Keluar Aplikasi");
        System.out.println("=================================================");
        System.out.print("Pilih Menu (1/2/3) : ");

        do {
            while (!in.hasNextInt()) {
                String input = in.next();
                System.out.printf("\"%s\" is not a valid number.\n", input);
                System.out.print("Pilih Menu (1/2/3) : ");
            }
            noMenu = in.nextInt();
        } while (noMenu < 0);
        setPilihMenu();
    }

    public void setPilihMenu() {
        BayarTolController dd = new BayarTolController();
        switch (noMenu) {
            case 1:
                dd.setMasukBayarTol();
                break;
            case 2:
                dd.getDataBayarTol();
                break;
            case 3:
                System.out.println("TERIMAKASIH ATAS LAYANAN ANDA :)");
                System.exit(0);
                break;
        }
    }
}


