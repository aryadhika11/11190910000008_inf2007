package id.aryadhika11.BayarTol.Controller;

import com.google.gson.Gson;
import id.aryadhika11.BayarTol.Model.BayarTol;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class BayarTolController {

    private static final String FILE = "C:\\Users\\Muhammad Arya Dhika\\Documents\\NetBeansProjects\\AplikasiBayarTol\\lib\\BayarTol.json";
    private BayarTol bayartol;
    private final Scanner in;
    private int golongan;
    private LocalDateTime waktuMasuk;
    private BigDecimal harga;
    private final DateTimeFormatter dateTimeFormat;
    private int pilihan;

    public BayarTolController() {
        in = new Scanner(System.in);
        waktuMasuk = LocalDateTime.now();
        dateTimeFormat = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
    }

    public void setMasukBayarTol() {
        BayarTol p = new BayarTol();
        waktuMasuk = LocalDateTime.now();

        System.out.println("** Jenis Golongan Kendaraan **");
        System.out.println("Golongan 1 : Sedan, Jip, Pick Up/Truk kecil, dan Bus");
        System.out.println("Golongan 2 : Truck dengan 2 Gandar");
        System.out.println("Golongan 3 : Truck dengan 3 Gandar");
        System.out.println("Golongan 4 : Truck dengan 4 Gandar");
        System.out.println("golongan 5 : Truck dengan 5 Gandar");
        System.out.println("golongan 6 : Truck dengan 6 Gandar");

        System.out.print("Masukkan golongan kendaraan : ");
        golongan = in.nextInt();

        if (golongan == 1) {
            harga = new BigDecimal(20000);
            System.out.println("golongan 1 = " + harga);
        } else if (golongan == 2) {
            harga = new BigDecimal(25000);
            System.out.println("golongan 2 =  " + harga);
        } else if (golongan == 3) {
            harga = new BigDecimal(30000);
            System.out.println("golongan 3 =  " + harga);
        } else if (golongan == 4) {
            harga = new BigDecimal(40000);
            System.out.println("golongan 4 =  " + harga);
        }else if (golongan == 5){
            harga = new BigDecimal(50000);
            System.out.println("golongan 5 = " + harga);
        }else if (golongan == 6){
            harga = new BigDecimal (55000);
            System.out.println("golongan 6 = " + harga);
        } else if (golongan > 6) {
            System.out.println("DATA TIDAK DITEMUKAN");
            setMasukBayarTol();
        }
        String formatWaktuMasuk = waktuMasuk.format(dateTimeFormat);
        System.out.println("Waktu Masuk : " + formatWaktuMasuk);

        p.setwaktumasuk(waktuMasuk);
        p.setharga(harga);
        p.setgolongan(golongan);
        System.out.println("Proses Bayar?");
        System.out.print("1) Ya, 2) Tidak : ");
        pilihan = in.nextInt();
        if (pilihan == 1) {
            p.setKeluar(true);
            setWriteBayarTol(FILE, p);
        } else {
            Menu m = new Menu();
            m.getMenuAwal();
        }
        System.out.println("Apakah mau Input kembali?");
        System.out.print("1) Ya, 2) Tidak : ");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            setMasukBayarTol();
        }
    }

    public void setWriteBayarTol(String file, BayarTol bayartol) {
        Gson gson = new Gson();

        List<BayarTol> bayartoll = getReadBayarTol(file);
        bayartoll.remove(bayartol);
        bayartoll.add(bayartol);

        String json = gson.toJson(bayartoll);
        try {
            FileWriter writer = new FileWriter(file);
            writer.write(json);
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(BayarTolController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<BayarTol> getReadBayarTol(String file) {
        List<BayarTol> bayartol = new ArrayList<>();

        Gson gson = new Gson();
        String line = null;
        try (Reader reader = new FileReader(file)) {
            BufferedReader br = new BufferedReader(reader);
            while ((line = br.readLine()) != null) {
                BayarTol[] ps = gson.fromJson(line, BayarTol[].class);
                bayartol.addAll(Arrays.asList(ps));
            }
            br.close();
            reader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BayarTolController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BayarTolController.class.getName()).log(Level.SEVERE, null, ex);
        }

        return bayartol;
    }

    public void getDataBayarTol() {
        List<BayarTol> bayartol = getReadBayarTol(FILE);
        Predicate<BayarTol> isKeluar = e -> e.isKeluar() == true;
        Predicate<BayarTol> isDate = e -> e.getwaktumasuk().toLocalDate().equals(LocalDate.now());

        List<BayarTol> pResults = bayartol.stream().filter(isKeluar.and(isDate)).collect(Collectors.toList());
        BigDecimal harga = pResults.stream()
                .map(BayarTol::getharga)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        System.out.println("Jenis golongan  Waktu Masuk \t\tBiaya");
        System.out.println("--------- \t----------- \t\t------------ \t\t-----");
        pResults.forEach((p) -> {
            System.out.println(p.getgolongan() + "\t\t" + p.getwaktumasuk().format(dateTimeFormat) + "\t" + p.getharga());
        });
        System.out.println("--------- \t----------- \t\t------------ \t\t-----");
        System.out.println("====================================");
        System.out.println("Pendapatan Total = Rp. " + harga);
        System.out.println("====================================");

        System.out.println("Apakah mau mengulanginya?");
        System.out.print("1) Ya, 2) Tidak : ");
        pilihan = in.nextInt();
        if (pilihan == 2) {
            Menu m = new Menu();
            m.getMenuAwal();
        } else {
            getDataBayarTol();
        }
    }

}
