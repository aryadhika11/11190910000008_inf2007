/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.BayarTol.Model;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class Info {

    private final String aplikasi = "Aplikasi Bayar Tol";
    private final String version = "Versi 1.O";

    public String getAplikasi() {
        return aplikasi;
    }

    public String getVersion() {
        return version;

    }

   
}
