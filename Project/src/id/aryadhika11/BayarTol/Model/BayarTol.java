/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package id.aryadhika11.BayarTol.Model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 *
 * @author Muhammad Arya Dhika
 */
public class BayarTol implements Serializable {

    private static final long serialVersionUID = -67564638752913469L;

    private int golongan;
    private LocalDateTime waktumasuk;
    private boolean keluar = false;
    private BigDecimal harga;
    
    public BayarTol (){
        
    }
    public BayarTol(int golongan, BigDecimal harga, LocalDateTime waktumasuk) {
        this.golongan = golongan;
        this.waktumasuk = waktumasuk;
        this.harga = harga;

    }

    public int getgolongan() {
        return golongan;
    }

    public void setgolongan(int golongan) {
        this.golongan = golongan;
    }

    public BigDecimal getharga() {
        return harga;
    }

    public void setharga(BigDecimal harga) {
        this.harga = harga;
    }

    public LocalDateTime getwaktumasuk() {
        return waktumasuk;
    }

    public void setwaktumasuk(LocalDateTime waktumasuk) {
        this.waktumasuk = waktumasuk;
    }

    public boolean isKeluar() {
        return keluar;
    }

    public void setKeluar(boolean keluar) {
        this.keluar = keluar;
    }

    @Override
    public String toString() {
        return "BayarTol{" + "golongan=" + golongan + ", harga =" + harga + ", waktumasuk=" + waktumasuk + ", keluar =" + keluar + '}';
    }
}
